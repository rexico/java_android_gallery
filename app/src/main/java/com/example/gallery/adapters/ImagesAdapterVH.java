package com.example.gallery.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gallery.R;
import com.example.gallery.models.Image;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImagesAdapterVH extends RecyclerView.Adapter<ImagesAdapterVH.ViewHolder> {
    public interface OnImagesAdapterListener {
        void onImageClick(int position);
    }
    private final LayoutInflater inflater;
    private final ArrayList<Image> images;
    private final Context context;
    private OnImagesAdapterListener onImagesAdapterListener;

    public ImagesAdapterVH(Context context, ArrayList<Image> images) {
        this.images = images;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        if(context instanceof OnImagesAdapterListener) {
            onImagesAdapterListener = (OnImagesAdapterListener) context;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.images_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Image img = images.get(position);
        Picasso.get().load(Uri.parse(img.getPath())).into(holder.imageView);
        holder.tvTitle.setText(img.getTitle());

        holder.imageView.setOnClickListener(v -> {
            onImagesAdapterListener.onImageClick(position);
        });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tvTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
        }
    }
}
