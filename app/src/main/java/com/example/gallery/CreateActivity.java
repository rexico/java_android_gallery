package com.example.gallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.CursorLoader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.gallery.models.Image;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.time.LocalDate;

public class CreateActivity extends AppCompatActivity {
    private static final int FILE_SELECT_CODE = 0;
    private static final String TAG = "FILE_CHOOSE";

    Image img;
    String path;
    int size;
    ImageView imageViewCreate;
    EditText etTitle;
    Button btnSave, btnCancell, btnChooseFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        etTitle = findViewById(R.id.etTitle);
        btnSave = findViewById(R.id.btnSave);
        btnCancell = findViewById(R.id.btnCancell);
        btnChooseFile = findViewById(R.id.btnChooseFile);
        imageViewCreate = findViewById(R.id.imageViewCreate);

        btnSave.setOnClickListener(v -> {
            img = new Image(
                    etTitle.getText().toString(),
                    path,
                    size
            );
            Intent intent = new Intent();
            intent.putExtra("img", img);
            setResult(RESULT_OK, intent);
            finish();
        });

        btnCancell.setOnClickListener(v -> {
            setResult(RESULT_CANCELED);
            finish();
        });

        btnChooseFile.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType( "image/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            try {
                startActivityForResult(
                        Intent.createChooser(intent, "Select image"),
                        FILE_SELECT_CODE);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "Please install a File Manager.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    path = uri.toString();
                    Cursor returnCursor =
                            getContentResolver().query(uri, null, null, null, null);

                    int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                    int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                    returnCursor.moveToFirst();

                    etTitle.setText(returnCursor.getString(nameIndex));
                    size = (int)returnCursor.getLong(sizeIndex)/1024;

                    Log.d(TAG, "File Uri: " + uri.toString());

                    Picasso.get().load(Uri.parse(path)).into(imageViewCreate);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}