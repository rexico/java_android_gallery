package com.example.gallery;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.gallery.adapters.ImagesAdapterVH;
import com.example.gallery.models.Image;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ImagesAdapterVH.OnImagesAdapterListener {
    ArrayList<Image> images = new ArrayList<>();
    RecyclerView recyclerView;
    ImagesAdapterVH adapterVH;
    Button btnCreate, btnLoad;
    //
    private static final String TAG = "myLogs";
    private final int ADD_REQUEST_CODE = 1;
    private final int EDIT_REQUEST_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        btnCreate = findViewById(R.id.btnCreate);
        btnLoad = findViewById(R.id.btnLoad);

        adapterVH = new ImagesAdapterVH(this, images);
        recyclerView.setAdapter(adapterVH);

        btnCreate.setOnClickListener(v -> {
            Intent intent = new Intent(this, CreateActivity.class);
            startActivityForResult(intent, ADD_REQUEST_CODE);
        });
        btnLoad.setOnClickListener(v -> {
            loadImagesFromAssets();
            adapterVH.notifyDataSetChanged();
        });
   }

    @Override
    public void onImageClick(int position) {
        Intent intent = new Intent(MainActivity.this, ViewImageActivity.class);
        intent.putParcelableArrayListExtra("images", MainActivity.this.images);
        intent.putExtra("position", position);
        startActivityForResult(intent, EDIT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ADD_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Image img = data.getParcelableExtra("img");
                    images.add(img);
                    adapterVH.notifyDataSetChanged();
                }
                break;
            case EDIT_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    ArrayList<Image> a = data.getParcelableArrayListExtra("imgs");
                    images.retainAll(a);
                    adapterVH.notifyDataSetChanged();
                }
                break;
        }
    }

    private void loadImagesFromAssets() {
        AssetManager AM = this.getAssets();
        try
        {
            String[] F = AM.list("");
            for (String f : F)
            {
                try
                {
                    String path = "file:///android_asset/" + f;
                    InputStream IS = AM.open(f);
                    int kb = IS.available()/1024;
                    images.add(new Image(f, path, kb));
                    IS.close();
                }
                catch (Exception e)
                {
                    Log.d(TAG, " Exception : " + e.getMessage());
                }
            }
        }
        catch (IOException ioe)
        {
            Log.d(TAG, "IOException : " + ioe.getMessage());
            Toast.makeText(this,
                    "IOException : \n" + ioe.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }

    }

}