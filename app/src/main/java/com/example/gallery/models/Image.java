package com.example.gallery.models;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class Image implements Parcelable {
    private String title;
    private String path;
    private int size;

    public Image(String title, String path, int size) {
        this.title = title;
        this.path = path;
        this.size = size;
    }
    public Image(Parcel in) {
        String[] data = new String[2];
        in.readStringArray(data);
        this.title = data[0];
        this.path = data[1];
        this.size = in.readInt();

    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] { title, path });
        dest.writeInt(size);
    }
}
