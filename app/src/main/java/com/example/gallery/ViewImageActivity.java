package com.example.gallery;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gallery.models.Image;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ViewImageActivity extends AppCompatActivity {

    TextView tvTitleV, tvPath, tvSize;
    ImageView imageViewImgs;
    ArrayList<Image> images;
    int position;
    Image img;
    Button btnDelete, btnBack;
    ImageButton btnLeft, btnRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);

        tvTitleV = findViewById(R.id.tvTitleV);
        tvPath = findViewById(R.id.tvPath);
        tvSize = findViewById(R.id.tvSize);
        imageViewImgs = findViewById(R.id.imageViewImgs);
        btnLeft = findViewById(R.id.btnLeft);
        btnDelete = findViewById(R.id.btnDelete);
        btnRight = findViewById(R.id.btnRight);
        btnBack = findViewById(R.id.btnBack);

        Bundle bundle = getIntent().getExtras();
        ArrayList<Image> a = bundle.getParcelableArrayList("images");
        images = a;
        position = bundle.getInt("position");

        btnDelete.setOnClickListener(v -> {
            images.remove(img);
            if(images.size() != 0) {
                positionIncrease();
                loadData();
            }
            else {
                backData();
            }
        });

        btnBack.setOnClickListener(v -> {
            backData();
        });

        btnLeft.setOnClickListener(v -> {
            positionDecrease();
            loadData();
        });

        btnRight.setOnClickListener(v -> {
            positionIncrease();
            loadData();
        });

        loadData();
    }

    private void loadData() {
        img = images.get(position);
        Picasso.get().load(Uri.parse(img.getPath())).into(imageViewImgs);
        tvTitleV.setText(img.getTitle());
        tvPath.setText(img.getPath());
        tvSize.setText(String.valueOf(img.getSize()) + " KB");
    }

    private void backData() {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra("imgs", images);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void positionDecrease() {
        if((position - 1) >= 0) {
            position--;
        }
        else {
            position = images.size() - 1;
        }
    }
    private void positionIncrease() {
        if((position + 1) < images.size()) {
            position++;
        }
        else {
            position = 0;
        }
    }

}